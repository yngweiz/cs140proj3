#ifndef USERPROG_PROCESS_H
#define USERPROG_PROCESS_H

#include "threads/thread.h"

tid_t process_execute (const char *file_name);
int process_wait (tid_t);
void process_exit (void);
void process_activate (void);
//Xiao
bool process_install_page(void *, void *, bool);
//XIao
#endif /* userprog/process.h */
