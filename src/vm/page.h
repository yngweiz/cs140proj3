#ifndef VM_PAGE_H
#define VM_PAGE_H


#include <list.h>
#include "filesys/file.h"
//#include "vm/swap.h"
//#include "vm/frame.h"

struct spt_elem{
  struct thread* owner;
 
  uint8_t *upage;
 
  struct file *fileptr;
  off_t ofs;
  uint32_t read_bytes;
  uint32_t zero_bytes;
  bool writable;
 
  void *kpage;
 
 
  bool needclose;
  bool needremove;
  int mapid;
 

  struct list_elem elem;
};
struct list_elem *page_find (void *);
#endif /* vm/page.h */
