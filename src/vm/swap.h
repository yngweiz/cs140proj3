#ifndef VM_SWAP_H
#define VM_SWAP_H

//#include "vm/frame.h"
//#include "vm/page.h"

#include <list.h>
#include "threads/palloc.h"
#include "threads/thread.h"
#include "devices/block.h"
#include "userprog/pagedir.h"

struct slot{
        //list
        struct list_elem elem;
        //virtual address
        uint8_t *upage;
        //partion virtual address
        tid_t pid;
        //starting block divide 4

        block_sector_t slid;
};

//init
void  swap_init(void);
//replace
void* evict(void *upage,struct thread* newown);
//write back
bool readback(void* upage,void* fp);
//find
struct list_elem *swap_find (void *upage);
#endif
