#include "vm/frame.h"
#include "vm/swap.h"
//#include "vm/page.h"

#include <list.h>
#include <stdio.h>
#include "threads/palloc.h"
#include "threads/thread.h"
#include "devices/block.h"
#include "userprog/pagedir.h"
#include "threads/vaddr.h"
static bool used[8192];
//initialize
/*try to solve some sync problems */
static struct lock block_lock;


void swap_init(void)
{
        int i;
        for(i=0;i<8192;i++)
                used[i]=false;
    lock_init(&block_lock);
}
struct list_elem *swap_find (void *upage){
  struct thread* cur=thread_current();
  struct slot* sp;
  struct list_elem *e;
  lock_acquire(&cur->swap_list_lock);
  /*  load page according to the spt, if not in spt, do nothing */
  for (e = list_begin (&cur->swapt); e != list_end (&cur->swapt);
         e = list_next (e))
      {
        sp = (struct slot *)list_entry (e, struct slot, elem);
        if(upage==sp->upage){
                  lock_release (&cur->swap_list_lock);
          return e;
        }
      }
  /*  not found */
  lock_release (&cur->swap_list_lock);
  return 0;
}
bool readback(void* upage,void* fp){
        struct thread* cur=thread_current();
        struct slot* sp;
        struct block *swapb=block_get_role(BLOCK_SWAP);
        struct list_elem *e;
        //struct frame* fp;
        block_sector_t tar;
        void* buf;
        int i;
        lock_acquire(&cur->swap_list_lock);
        for (e = list_begin (&cur->swapt); e != list_end (&cur->swapt);
         e = list_next (e))
        {
                sp=list_entry (e, struct slot, elem);
                if(sp->pid==cur->tid&&sp->upage==upage)
                {                      
                                tar=sp->slid*8;
                                buf=fp;
                                //write back
                                for(i=0;i<8;i++)
                                {
                                        block_read (swapb,tar,buf);
                                        //hex_dump(buf,buf,100,1);
                                        used[tar]=false;
                                        tar++;
                                        buf=buf+512;
                                }
                                //set mapping
                                pagedir_set_page (cur->pagedir,upage,fp,true);  
                                //delete sp
                                list_remove(sp);
                                lock_release (&cur->swap_list_lock);
                                return true;
                }
        }
        lock_release (&cur->swap_list_lock);
        return false;  
}
        
