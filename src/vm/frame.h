//#ifndef VM_FRAME_H
//#define VM_FRAME_H
#include <list.h>
#include "threads/thread.h"

/*  a basic frame elem, need more vars. */
struct frame{
  /* physics address */
  void* paddr;
  /* current working @some user page(virtual addr) */
  void* upage;
  /* the owner thread */
  struct thread* owner;
  /* protect bytes
   * char flag;*/
 
  int recent;
 
  struct list_elem elem;
  };

/* : a list init, should be called at system start */
void frame_table_init(void);
/* : etc */
bool frame_table_full (void);
/* : frame_get must be called with an upage */
void* frame_get (bool,void*);
void frame_free (void *);
struct frame* frame_find (void *);
struct frame* LRU(void);
void changerec(void);
//#endif /* vm/frame.h */

